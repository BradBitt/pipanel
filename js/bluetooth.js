const bluetooth = require('node-bluetooth');
 
// create bluetooth device instance
const device = new bluetooth.DeviceINQ();

$(document).ready(function() {
    findDevices();
});

function findDevices() {
    // Hides the error message (might be being shown)
    $("#bluetooth-error").css("display","none");
    $("#bluetooth-device-list").css("display","flex");

    // Removes all the children from the list
    $("#bluetooth-device-list").empty();

    // When the 'found' function is called, the following script is used.
    // Finds all bluetooth devices and adds them to the html
    device.on('found', function found(address, name){

        // Adds the device to the list if valid.
        addDevice(address, name);

        // Removes the Error message if it is visible.
        if ($('#bluetooth-error').css('display') != 'none') {
            $('#bluetooth-error').css('display','none');
            $("#bluetooth-device-list").css("display","flex");
        }
        
    }).scan();

    // If no bluetooth devices were found then display error message.
    if ($("#bluetooth-device-list").children().length == 0) {
        $("#bluetooth-error").css("display","block");
        $("#bluetooth-error .card-text").text("Could not find any bluetooth devices to connect to! Make sure you have bluetooth turned on.")
        $("#bluetooth-device-list").css("display","none");
    }
}

function addDevice(address, name) {

    // Checks if there is a device in the list with the same name already
    // -1 is returned if string could not be found
    $("#bluetooth-device-list").children('input').each(function () {
        if (this.text.toLowerCase().indexOf(name) >= 0) {
            return;
        }
    });

    // TODO: Add symbol based on if it is connected or not.
    var iconType = "fa-circle"; // A device that is not connected

    // Adds the blue tooth device to the list.
    $("#bluetooth-device-list").append("<li class='list-group-item'><p>" + 
            name + 
            "</p><a href='#' class='btn bluetooth-button'><i id='" + name + "-id" +
            "' class='fa " + iconType + "'" + "></i></a></li>");

    // Function is called when the user clicks this button
    $("#" + name + "-id").click(connectDevice(address));
}

function connectDevice(address) {

    // make bluetooth connect to remote device
    // port should be 8080??
    /*bluetooth.connect(address, 8080, function(err, connection){
        
        if(err) return console.error("didnt work");
    
        connection.on('data', (buffer) => {
            console.log('received message:', buffer.toString());
        });
    });*/
}

// Exports the function so other files can use the method.
module.exports.findDevices = findDevices;  