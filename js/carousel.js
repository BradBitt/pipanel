// TODO remove swipe from carousel and add it to a seperate div that floats above the carousel. 
// Give it a higher z-index than the carosuel but lower than the settings menu. 
// Give the volume bar a higher z index than the swipe div.
$( document ).ready(function() {
    $(".carousel").swipe({

        swipeLeft: function(event, direction, distance, duration, fingerCount, fingerData) {

            // Swipes the Carousel left
            if (direction == 'left') $(this).carousel('next');

        },threshold:75
        
    });
    $(".carousel").swipe({

        swipeRight: function(event, direction, distance, duration, fingerCount, fingerData) {

            // Swipes the Carousel right
            if (direction == 'right') $(this).carousel('prev');

        },threshold:75

    });
    $(".carousel").swipe({

        swipeDown: function(event, direction, distance, duration, fingerCount, fingerData) {

            // Swiping the Carousel down make the settings menu appear.
            $(".settings-container").animate({top: "0%"});

        },threshold:75

    });
});

