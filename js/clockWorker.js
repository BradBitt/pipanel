// This is the worker thread.
// Everything done here is a seperate thread to the main thread.
this.onmessage = function(e) {
    if(e.data.date !== undefined) { 
        var dateNew = new Date();
        var time = dateNew.getHours() + ":" + dateNew.getMinutes() + ":" + dateNew.getSeconds();
        this.postMessage( {result: time} );
    }
}