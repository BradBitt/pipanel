$(document).ready(function() {
    getTime();
});

function getTime() {

    var currentDate = new Date();
        
    // This creates a seperate process that runs alongside the main process.
    if (window.Worker) {
        var myWorker = new Worker("./js/clockWorker.js");

        var message = { date: {date1:currentDate} };
        
        // This sends off the data to be computed in a seperate thread.
        myWorker.postMessage(message);
        
        // This is what is returned and everything in here is also done in a
        // seperate thread. I have used recursion to get it to loop forever.
        // Can't update the DOM inside the worker thread ("worker.js").
        // Editing DOM has to be done in the onmessage function below.
        myWorker.onmessage = function(e) {

            // Gets the time and displays it.
            $("#clock").text(event.data.result);
            
            setTimeout(function(){ 
                myWorker.postMessage(message); 
            }, 1000);
        }
    }
}