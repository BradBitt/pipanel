var radio = require("radio-stream");

$(document).ready(function() {
    play();
    setVolume();
});

function play() {

    // make sure its a http and not https.
    // Make sure it ends in a .mp3 file.
    var stream = radio.createReadStream("http://stream-mz.planetradio.co.uk/magicnational.mp3");

    stream.on("connect", function() {
        console.log("Radio Stream connected!");
        console.log(stream.headers);
    });

    stream.on("metadata", function(metadata) {
        updateRadioTitle(metadata);
    });
}

// Toggles the play or pause of current song.
function togglePlay() {

    if($('#radio-toggle-button > i').hasClass("fa-play")) {
        $('#radio-player').get(0).play();
        $("#radio-toggle-button > i").removeClass('fa-play');
        $("#radio-toggle-button > i").addClass('fa-pause');
    } else {
        $('#radio-player').get(0).pause();
        $("#radio-toggle-button > i").addClass('fa-play');
        $("#radio-toggle-button > i").removeClass('fa-pause');
    }
}

// Uses regex to get the name of the song and artist and then displays it in html.
function updateRadioTitle(metadata) {
    
    var reg = /StreamTitle='([A-Z0-9a-z\s-'!"£$%^&*():;{}~#?<>,.]*)';/g;
    var match = reg.exec(metadata);

    if (match != null) {
        // if no song is currently being played
        if (match[1] == '') {
            $('#current-song').text("Advert Playing...");
        } else {
            $('#current-song').text(match[1]);
        }
    }
}

function setVolume() {
    var slider = document.getElementById("radio-volume");
    slider.oninput = function() {
        var volumeFloat = parseFloat(this.value);
        $('#radio-player').get(0).volume = (volumeFloat / 100);

    }
}

module.exports.togglePlay = togglePlay;