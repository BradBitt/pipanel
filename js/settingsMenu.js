$( document ).ready(function() {
    $(".settings-container").swipe({

        swipeUp: function(event, direction, distance, duration, fingerCount, fingerData) {

            // Swiping the settings menu up to make the settings menu disappear.
            if (direction == 'up') {
                $(".settings-container").animate({top: "-100%"});
            }

        },threshold:75
        
    });
});